package org.pyerohdc.olga.launcher

import java.io.File
import java.io.InputStream
import java.time.Instant
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.ZoneId
import java.util.*

class LaunchProperties(private val properties: Properties) {

    var javaVersion: String?
        get() = properties["java.version"] as? String
        set(value) = run { properties["java.version"] = value }

    var olgaVersion: String?
        get() = properties["olga.version"] as? String
        set(value) = run { properties["olga.version"] = value }

    var lastUpdated: LocalDateTime?
        get() = (properties["last.updated"] as? String)?.let {
            LocalDateTime.ofInstant(
                Instant.ofEpochSecond(it.toLong()),
                ZoneId.systemDefault()
            )
        }
        set(value) = run { properties["last.updated"] = value?.toInstant(OffsetDateTime.now().offset)?.epochSecond.toString() }

    fun load(inputStream: InputStream) {
        properties.load(inputStream)
    }

    fun writeProps(workFolder: File) {
        val propsFileName = "olga-launcher.properties"
        workFolder
            .toPath()
            .resolve(propsFileName)
            .toFile()
            .outputStream()
            .use { properties.store(it, null) }
    }

}
