package org.pyerohdc.olga.launcher

interface FileLink {
    val url: String
    val name: String
}
