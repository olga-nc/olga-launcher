package org.pyerohdc.olga.launcher

import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.*
import javax.swing.border.EmptyBorder

class ProgressFrame() {

    private val frame: JFrame

    private val _progress: JProgressBar

    private val progressLabel: JLabel

    init {
        frame = JFrame()

        frame.iconImage = ImageIcon(javaClass.classLoader
            .getResourceAsStream("olga-launcher.png")
            ?.readAllBytes())
            .image

        frame.defaultCloseOperation = WindowConstants.DO_NOTHING_ON_CLOSE
        frame.isResizable = false
        frame.title = "OLGA Launcher"
        frame.setLocationRelativeTo(null)
        frame.layout = BorderLayout(5, 5)
        (frame.contentPane as JComponent).border = EmptyBorder(5, 5, 5, 5)

        _progress = JProgressBar(0, 100)
        _progress.minimumSize = Dimension(300, 15)
        _progress.preferredSize = _progress.minimumSize
        frame.contentPane.add(_progress, BorderLayout.NORTH)

        progressLabel = JLabel("progression", SwingConstants.CENTER)
        frame.contentPane.add(progressLabel, BorderLayout.SOUTH)

        frame.pack()

        progressLabel.text = ""
    }

    var isVisible: Boolean
        get() = frame.isVisible
        set(visible) = run { frame.isVisible = visible }

    var progressText: String
        get() = progressLabel.text
        set(text) = run { progressLabel.text = text }

    var progress: Int
        get() = _progress.value
        set(value) = run { _progress.value = value }

    fun dispose() = frame.dispose()

}
