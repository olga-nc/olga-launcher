package org.pyerohdc.olga.launcher

import java.io.PrintWriter
import java.io.StringWriter
import javax.swing.JOptionPane
import javax.swing.UIManager
import kotlin.system.exitProcess

class Launcher {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
            val progressFrame = ProgressFrame()

            Thread.setDefaultUncaughtExceptionHandler { _, e ->
                progressFrame.dispose()

                val errors = StringWriter()
                e.printStackTrace(PrintWriter(errors))

                JOptionPane.showMessageDialog(
                    null, errors.toString(),
                    "OLGA Launcher",
                    JOptionPane.ERROR_MESSAGE
                )
                e.printStackTrace()
            }

            val manager = LaunchManager()

            val workFolder = manager.getOrCreateWorkFolder()

            val props = manager.getOrCreateProperties(workFolder)

            val olgaJar = manager.getOrCreateOlgaJar(workFolder, props, progressFrame)

            val zuluFolder = manager.getOrCreateZuluFolder(workFolder, props, progressFrame)

            progressFrame.isVisible = false
            progressFrame.dispose()

            Runtime.getRuntime().exec("${zuluFolder.toPath().resolve("bin").resolve("java")} -jar ${olgaJar.toPath()}")

            exitProcess(0)

            // récupérer le dossier d'exécution du launcher, et remonter d'un niveau, pour exécuter OLGA "à côté"
            // vérifier si le dossier applicatif existe. Si non, le créer. Aller dedans.
            // vérifier si le JRE JFX Zulu existe. Si non, le télécharger depuis leur site (avec JSoup) et le dézipper (avec Zip4J)
            // vérifier si OLGA est présent. Si non, le télécharger depuis gitlab. Si oui, vérifier que c'est la dernière version (en parsant le pom de la branche master) et comparer à la version du fichier de properties.
            // Du coup, si ce n'est pas la dernière version, télécharger la nouvelle version depuis Gitlab.
        }
    }

}
