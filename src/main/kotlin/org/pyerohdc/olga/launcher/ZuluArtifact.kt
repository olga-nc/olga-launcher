package org.pyerohdc.olga.launcher

data class ZuluArtifact(
    override val url: String,
    override val name: String
) : FileLink {
    fun toJre() = copy(url = toJre(url), name = toJre(name))

    private fun toJre(str: String) = str.replace("jdk", "jre")
}
