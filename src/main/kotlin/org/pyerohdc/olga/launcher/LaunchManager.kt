package org.pyerohdc.olga.launcher

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import net.lingala.zip4j.ZipFile
import okhttp3.ConnectionSpec
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.TlsVersion
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.util.Locale
import java.util.Properties
import kotlin.math.ln
import kotlin.math.pow


class LaunchManager {

    private val httpClient = OkHttpClient.Builder().let { builder ->
        builder.connectionSpecs(
            listOf(
                ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .build()
            )
        )
        builder.build()
    }

    private val moshi = Moshi.Builder()
        .add(OffsetDateTimeJsonAdapter())
        .add(KotlinJsonAdapterFactory())
        .build()

    fun getOrCreateWorkFolder(): File {
        val workFolder =
            File(javaClass.protectionDomain.codeSource.location.toURI()).toPath().parent.parent.resolve("OLGA").toFile()
        if (!workFolder.exists()) {
            workFolder.mkdir()
        }
        return workFolder
    }

    fun getOrCreateProperties(workFolder: File): LaunchProperties {
        val propsFileName = "olga-launcher.properties"
        val propsFile = workFolder
            .listFiles { dir, name -> dir.toPath().resolve(name).toFile().isFile && name == propsFileName }
            ?.getOrNull(0)
        val props = LaunchProperties(Properties())
        if (propsFile != null) {
            propsFile.inputStream().use { props.load(it) }
        } else {
            props.javaVersion = "8"
            props.writeProps(workFolder)
        }

        return props
    }

    fun getOrCreateZuluFolder(workFolder: File, props: LaunchProperties, progressFrame: ProgressFrame): File {
        if (findZuluFolder(workFolder).isEmpty()) {
            val request = buildZuluRequest(props)

            httpClient.newCall(request).execute().useSuccessfulOnly { response ->
                val artifact = getZuluArtifact(response)
                val fileRequest = Request.Builder()
                    .url(artifact.url)
                    .build()
                httpClient.newCall(fileRequest).execute().useSuccessfulOnly { fileResponse ->
                    val downloadedArtifact = downloadFile(
                        "Téléchargement du JRE",
                        fileResponse,
                        workFolder,
                        artifact,
                        progressFrame
                    )
                    extractDownloadedJre(downloadedArtifact)
                }
            }
        }
        return findZuluFolder(workFolder).first()
    }

    fun getOrCreateOlgaJar(workFolder: File, props: LaunchProperties, progressFrame: ProgressFrame): File {
        val request = buildOlgaRequest()

        val releases = httpClient.newCall(request).execute()
            .useSuccessfulOnly { listReleasesResponse ->
                val json = listReleasesResponse.body!!.string()
                val listType = Types.newParameterizedType(List::class.java, GitlabRelease::class.java)
                val adapter = moshi.adapter<List<GitlabRelease>>(listType)
                adapter.fromJson(json)!!
            }

        if (releases.isEmpty()) throw RuntimeException("Aucun livrable n'est disponible !")

        val latestRelease = releases.maxBy { it.released_at }!!

        if (findOlgaFile(workFolder).isEmpty() || latestRelease.name != props.olgaVersion!!) {
            props.lastUpdated = LocalDateTime.now()
            props.olgaVersion = latestRelease.name
            props.writeProps(workFolder)

            val link = latestRelease.assets.links.first()

            val linkRequest = Request.Builder()
                .url(link.url)
                .build()

            return httpClient.newCall(linkRequest).execute().useSuccessfulOnly { fileResponse ->
                downloadFile(
                    "Téléchargement d'OLGA",
                    fileResponse,
                    workFolder,
                    link,
                    progressFrame
                )
            }
        } else {
            return findOlgaFile(workFolder).first()
        }
    }

    private fun buildZuluRequest(props: LaunchProperties): Request {
        val request: Request

        val isX64 = System.getProperty("os.arch").contains("amd64")
        val osName = System.getProperty("os.name").toLowerCase(Locale.ENGLISH)
        val os = when {
            osName.contains("win") -> "windows"
            osName.contains("mac") -> "macos"
            osName.contains("sunos") -> "solaris"
            listOf("nix", "nux", "aix").contains(osName) -> "linux"
            else -> throw UnsupportedOperationException("System $osName not supported")
        }
        request = Request.Builder()
            .url(
                HttpUrl.Builder()
                    .scheme("https")
                    .host("api.azul.com")
                    .addPathSegment("zulu")
                    .addPathSegment("download")
                    .addPathSegment("community")
                    .addPathSegment("v1.0")
                    .addPathSegment("bundles")
                    .addPathSegment("latest")
                    .addQueryParameter("ext", "zip")
                    .addQueryParameter("bundle_type", "jre")
                    .addQueryParameter("features", "fx")
                    .addQueryParameter("os", os)
                    .addQueryParameter("arch", "x86")
                    .addQueryParameter("hw_bitness", if (isX64) "64" else "32")
                    .addQueryParameter("jdk_version", props.javaVersion!!)
                    .build()
            )
            .build()
        return request
    }

    private fun buildOlgaRequest(): Request {
        return Request.Builder()
            .url(
                HttpUrl.Builder()
                    .scheme("https")
                    .host("gitlab.com")
                    .addPathSegment("api")
                    .addPathSegment("v4")
                    .addPathSegment("projects")
                    .addPathSegment("13791030") // id projet OLGA
                    .addPathSegment("releases")
                    .build()
            )
            .build()
    }


    private fun getZuluArtifact(response: Response): ZuluArtifact {
        val json = response.body!!.string()

        return moshi.adapter(ZuluArtifact::class.java)
            .fromJson(json)!!
            .toJre()
    }

    private fun downloadFile(
        progressText: String,
        fileResponse: Response,
        workFolder: File,
        artifact: FileLink,
        progressFrame: ProgressFrame
    ): File {
        val progressFormat = "$progressText... %s/%s (%s/s)"
        val fileSize = fileResponse.header("Content-Length")!!.toLong()
        val downloadedArtifact = workFolder.toPath().resolve(artifact.name).toFile()
        val downloadedArtifactOS = downloadedArtifact.outputStream()
        fileResponse.body!!.byteStream().use { responseIS ->
            downloadedArtifactOS.use {
                progressFrame.isVisible = true
                progressFrame.progress = 0
                responseIS.copyTo(it) { bytesFromLastSecond, totalBytesCopied ->
                    progressFrame.progressText =
                        progressFormat.format(
                            humanReadableByteCount(totalBytesCopied),
                            humanReadableByteCount(fileSize),
                            humanReadableByteCount(bytesFromLastSecond)
                        )
                    progressFrame.progress = ((totalBytesCopied / fileSize.toDouble()) * 100).toInt()
                }
            }
        }
        return downloadedArtifact
    }

    private fun extractDownloadedJre(downloadedArtifact: File) {
        ZipFile(downloadedArtifact).extractAll(downloadedArtifact.toPath().parent.toString())
        downloadedArtifact.delete()
    }

    private fun humanReadableByteCount(bytes: Long, si: Boolean = false): String {
        val unit = if (si) 1000 else 1024
        if (bytes < unit) return "$bytes B"
        val exp = (ln(bytes.toDouble()) / ln(unit.toDouble())).toInt()
        val pre = (if (si) "kMGTPE" else "KMGTPE")[exp - 1] + if (si) "" else "i"
        return String.format("%.1f %sB", bytes / unit.toDouble().pow(exp.toDouble()), pre)
    }


    private fun findFilesInFolder(workFolder: File, predicate: (dir: File, name: String) -> Boolean) =
        workFolder.listFiles(predicate)!!

    private fun findZuluFolder(workFolder: File) = findFilesInFolder(workFolder) { dir, name ->
        dir.resolve(name).isDirectory
                && name.contains("zulu", true)
    }

    private fun findOlgaFile(workFolder: File) = findFilesInFolder(workFolder) { dir, name ->
        "olga.jar" == name && dir.resolve(name).isFile
    }

    private fun InputStream.copyTo(
        out: OutputStream,
        updateSpeed: (bytesFromLastSecond: Long, totalBytesCopied: Long) -> Unit
    ): Long {
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        var bytes = read(buffer)

        var t0 = Instant.now()
        var bytesCopied: Long = 0
        var bytesCopiedFromT0: Long = 0

        while (bytes >= 0) {
            out.write(buffer, 0, bytes)
            bytesCopied += bytes
            bytesCopiedFromT0 += bytes

            if (Duration.between(t0, Instant.now()).toMillis() >= 1000) {
                updateSpeed(bytesCopiedFromT0, bytesCopied)
                t0 = Instant.now()
                bytesCopiedFromT0 = 0
            }

            bytes = read(buffer)
        }
        return bytesCopied
    }

}

inline fun <R> Response.useSuccessfulOnly(callable: (Response) -> R): R {
    return this.use {
        if (it.isSuccessful) {
            callable(it)
        } else {
            throw RuntimeException("Appel échoué : ${it.code} ${it.body?.string()}")
        }
    }
}
