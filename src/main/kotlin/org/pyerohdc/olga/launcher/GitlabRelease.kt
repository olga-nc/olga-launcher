package org.pyerohdc.olga.launcher

import java.time.OffsetDateTime

data class GitlabRelease(
    val name: String,
    val released_at: OffsetDateTime,
    val assets: GitlabAssets
)

data class GitlabAssets(val links: List<GitlabLink>)

data class GitlabLink(
    val id: Int,
    override val name: String,
    override val url: String
) : FileLink
